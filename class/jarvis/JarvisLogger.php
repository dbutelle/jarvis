<?php

namespace jarvis;
class JarvisLogger
{
    public function generateLoginForm(string $action): void
    {
        echo "<form method='post' action=$action id='login-form' >
        <legend style='text-align: :center;'>Please login</legend>
        <input type='text' name='username' placeholder='username'>
        <input type='password' name='password' placeholder='password'>
        <input type='submit' value='login' name='login' id='login'>
    </form>";

    }

    public function log(string $username, string $password): array
    {
        if ($username == 'stark' && $password == 'warmachinerox') {
            $granted = true;
            $nick = $username;
        } else {
            $granted = false;
            $nick = null;
        }
        if (empty($username)) {
            $error = 'username is empty';
            echo "<div class='error'> $error </div>";
        } elseif (empty($password)) {
            $error = 'password is empty';
            echo "<div class='error'> $error </div>";
        } else {
            if (!$granted) {
                $error = 'authentication failed';
                echo "<div class='error'> $error </div>";
            } else {
                $error = '';
            }
        }
        return [$granted, $nick, $error];
    }
}

?>