<?php
require __DIR__ . "/class/Autoloader.php" ;
Autoloader::register() ;
use jarvis\JarvisLogger;
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css"/>
    <title>Jarvis Interface</title>
</head>
<header>
    <?php
    include('header.php');
    ?>
</header>
<body>
<div id="main-content">
    <?php
    $form = new JarvisLogger();
    if (!empty($_POST)) {

        $username = htmlspecialchars($_POST['username']);
        $password = htmlspecialchars($_POST['password']);
        $array = $form->log($username, $password);
        if ($array[0] == true) {
            ?>
            <script>
                let logout = document.getElementById('logout')
                logout.classList.toggle('hidden');
            </script>
            <?php
            echo "<h1 style='font-size:8em; color:lime'>Hello tony</h1>";
        }else{
            $form->generateLoginForm('index.php');
        }
    } else {
        $form->generateLoginForm('index.php');
    }
    ?>
</div>
</body>
<footer>
    <?php
    include('footer.php');
    ?>
</footer>
</html>
